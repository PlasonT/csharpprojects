﻿namespace Task_2
{
    class Coordinates
    {
        #region Point A

        // Координаты ползуна.
        private static int x_A;
        public int X_A
        {
            get => x_A;
            set => x_A = value;
        }

        private static int y_A;
        public int Y_A
        {
            get => y_A;
            set => y_A = value;
        }

        #endregion

        #region Point B

        // Координаты шарнира B
        private static int x_B;
        public int X_B
        {
            get => x_B;
            set => x_B = value;
        }

        private static int y_B;
        public int Y_B
        {
            get => y_B;
            set => y_B = value;
        }

        #endregion

        #region Point C

        // Координаты шарнира C
        private static int x_C;
        public int X_C
        {
            get => x_C;
            set => x_C = value;
        }

        private static int y_C;
        public int Y_C
        {
            get => y_C;
            set => y_C = value;
        }

        #endregion

        #region Point D

        // Координаты шарнира D
        private static int x_D;
        public int X_D
        {
            get => x_D;
            set => x_D = value;
        }

        private static int y_D;
        public int Y_D
        {
            get => y_D;
            set => y_D = value;
        }

        #endregion

    }
}
