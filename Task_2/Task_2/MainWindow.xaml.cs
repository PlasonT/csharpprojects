﻿using System;
using System.Drawing;
using System.Windows;

using System.Windows.Forms;
using System.Windows.Input;
using OpenTK.Input;

using OpenTK.Graphics.OpenGL;

namespace Task_2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //System.Windows.Point firstPoint = new System.Windows.Point();        

        #region Initial Fields

        //bool isClicked = false;         // идентификатор нажатия кнопки мыши 

        //int Xmax = 20;                  // максимальное значение Х-координаты ползуна (правый ход)      
        //int Xmin = -20;                 // максимальное значение Х-координаты ползуна (левый ход) 

        int rotateAngle_B = 0;         // угол поворота звена BC + CD вокруг шарнира B
        int rotateAngle_C = 0;         // угол поворота звена CD вокруг шарнира С

        int firstPointA = -5;           // Х - координата первой точки ползуна A
        int secondPointA = 10;          // Х - координата второй точки ползуна A
        int thirdPointA = 10;           // Х - координата третьей точки ползуна A
        int fourthPointA = -5;          // Х - координата четвертой точки ползуна A

        int firstPointC = 11;           // X-координата точки C
        int secondPointB = 2;           // Х-координата точки B 
        int secondPointY_B = 8;

        int firstPointLeft = -25;       // Х - координата первой точки (левая направляющая ползуна)
        int secondPointLeft = -5;       // Х - координата второй точки (левая направляющая ползуна)

        int firstPointRight = 30;        // Х - координата первой точки (правая направляющая ползуна)
        int secondPointRight = 10;       // Х - координата второй точки (правая направляющая ползуна)

        // Имитация управления мышкой.
        int mousePos = 15; // перемещение ползуна влево-вправо; 

        public object MousePosition { get; private set; }

        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }

        private void RenderCanvas_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            #region Display setup

            // Очищаем буфер.
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Настройки вида.
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();           

            // Размер окна.
            GL.Viewport(0, 0, renderCanvas.Width, renderCanvas.Height);

            // Задаем координатную сетку.
            GL.Ortho(-50.0, 50.0, -50.0, 50.0, -50.0, 50.0);

            #endregion

            //================================ Рисуем механизм ====================================//

            #region Draw graphics            

            #region Napravlaushaya

            //============== Рисуем левую направляющую ползуна ====================// 

            //var mouse = OpenTK.Input.Mouse.GetState();
            //int x = mouse.X;
            //int y = mouse.Y;


            // Здаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);            
            GL.Color3(Color.Black);
            GL.Vertex2(firstPointLeft, -3);
            GL.Vertex2(secondPointLeft + mousePos, -3);
            GL.End();

            //============== Рисуем правую направляющую ползуна ====================//

            // Задаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(firstPointRight, -3);
            GL.Vertex2(secondPointRight + mousePos, -3);
            GL.End();

            #endregion

            #region Opora

            //============== Рисуем левую направляющую вниз ====================//

            // Задаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(-25, -3);
            GL.Vertex2(-25, -25);
            GL.End();

            //============== Рисуем правую направляющую вниз ====================//

            // Задаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(30, -3);
            GL.Vertex2(30, -25);
            GL.End();

            //============== Рисуем левую опору ====================//

            // Задаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(-30, -25);
            GL.Vertex2(-20, -25);
            GL.End();

            //============== Рисуем правую опору ====================//

            // Задаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(35, -25);
            GL.Vertex2(25, -25);
            GL.End();

            //============== Рисуем штриховку левой опоры ====================//

            // Задаем толщину линии.
            GL.LineWidth(3);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(-29, -25);
            GL.Vertex2(-26, -27);
            GL.End();

            //-------------------------------------------------------------//

            // Задаем толщину линии.
            GL.LineWidth(3);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(-26, -25);
            GL.Vertex2(-23, -27);
            GL.End();

            //-------------------------------------------------------------//

            // Задаем толщину линии.
            GL.LineWidth(3);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(-23, -25);
            GL.Vertex2(-20, -27);
            GL.End();

            //============== Рисуем штриховку правой опоры ====================//

            // Задаем толщину линии.
            GL.LineWidth(3);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(26, -25);
            GL.Vertex2(29, -27);
            GL.End();

            //-------------------------------------------------------------//

            // Задаем толщину линии.
            GL.LineWidth(3);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(29, -25);
            GL.Vertex2(32, -27);
            GL.End();

            //-------------------------------------------------------------//

            // Задаем толщину линии.
            GL.LineWidth(3);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);
            GL.Vertex2(32, -25);
            GL.Vertex2(35, -27);
            GL.End();

            #endregion 

            #region Polzun

            //======================== Движение механизма ==========================//
            //Coordinates coordinates = new Coordinates();

            // Движение ползуна А вдоль своей направляющей            
            GL.PushMatrix();            
            GL.Translate(mousePos, 0, 0);            

            //=============== Рисуем ползун =======================//

            GL.Begin(PrimitiveType.LineLoop);
            GL.LineWidth(2);
            GL.Color3(Color.Black);
            GL.Vertex2(firstPointA, 0);
            GL.Vertex2(secondPointA, 0);
            GL.Vertex2(thirdPointA, -5);
            GL.Vertex2(fourthPointA, -5);
            GL.End();

            #endregion

            #region Sharnir B

            //============== Рисуем шарнир В ====================//           

            #region Parameters

            double radius_B = 1.5;
            double initial_X_B = 2;
            double initial_Y_B = 8;

            //double delta = 0.5 * Math.Cos(rotateAngle_B);

            #endregion

            GL.Begin(PrimitiveType.LineLoop);
            GL.Color3(Color.Black);

            for (int i = 0; i < 360; i++)
            {
                GL.Vertex2(initial_X_B + (Math.Cos(i) * radius_B), initial_Y_B + (Math.Sin(i) * radius_B));
            }
            GL.End();

            //============== Рисуем левую опору шарнира В ====================//
            
            // Задаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);   // текущий цвет примитивов
            GL.Vertex2(0.5, 7);
            GL.Vertex2(-2, -0.2);
            GL.End();

            //============== Рисуем правую опору шарнира В ====================//

            // Задаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black);   // текущий цвет примитивов
            GL.Vertex2(3.5, 7);
            GL.Vertex2(6, -0.2);
            GL.End();           

            //====================== Вращение плеча BC =======================//

            // Вращение плеча вокруг шарнира В
            GL.PushMatrix();       // сохраняем текущее состояние
            GL.Rotate(rotateAngle_B, 0, 0, 1);            

            //============== Рисуем плечо ВС ====================//  

            // Задаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black); // текущий цвет примитивов
            GL.Vertex2(firstPointC, 20);
            GL.Vertex2(secondPointB, secondPointY_B);
            GL.End();           

            #endregion

            #region Sharnir C

            //============== Рисуем шарнир С ====================//             

            #region Parameters

            double radius_С = 1.5;
            double initial_X_С = 12;
            double initial_Y_С = 21;

            #endregion

            GL.Begin(PrimitiveType.LineLoop);
            GL.Color3(Color.Black); // текущий цвет примитивов

            for (int i = 0; i < 360; i++)
            {
                GL.Vertex2(initial_X_С + (Math.Cos(i) * radius_С), initial_Y_С + (Math.Sin(i) * radius_С));
            }
            GL.End();

            //============== Вращение плеча =======================// 

            // Вращение плеча CD вокруг шарнира С
            GL.PushMatrix();
            GL.Rotate(rotateAngle_C, 0, 0, 1);

            //============== Рисуем плечо СD ====================//
            // Задаем толщину линии.
            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Black); // текущий цвет примитивов
            GL.Vertex2(26.5, 21);
            GL.Vertex2(13.5, 21);
            GL.End();

            #endregion

            #region Sharnir D
            //============== Рисуем шарнир D ====================//

            #region Parameters

            double radius_D = 1.5;
            double initial_X_D = 28;
            double initial_Y_D = 21;

            #endregion

            GL.Begin(PrimitiveType.LineLoop);
            GL.Color3(Color.Black);  // текущий цвет примитивов

            for (int i = 0; i < 360; i++)
            {
                GL.Vertex2(initial_X_D + (Math.Cos(i) * radius_D), initial_Y_D + (Math.Sin(i) * radius_D));
            }
            GL.End();

            #endregion

            #endregion

            //=============================================================================//

            renderCanvas.SwapBuffers();
        }

        private void WindowsFormsHost_Initialized(object sender, EventArgs e)
        {
            renderCanvas.MakeCurrent();
        }

        private void RenderCanvas_Load(object sender, EventArgs e)
        {
            // Задаем цвет фона экрана.
            GL.ClearColor(Color.White);
        }

        private void RenderCanvas_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //System.Windows.Point FirstPoint = new System.Windows.Point(e.X, e.Y);            

            //System.Windows.Point getpos = PointToScreen(System.Windows.Input.Mouse.GetPosition(this));


            //if (e.Button == MouseButtons.Left)
            //{
            //    Increment rectangle-location by mouse-location delta.
            //   Rectangle.X += e.X - FirstPoint.X;
            //    Rectangle.Y += e.Y - FirstPoint.Y;

            //    Re - calibrate on each move operation.
            //    FirstPoint = new MovePoint { X = e.X, Y = e.Y };

            //    RenderCanvas.Invalidate();
            //}

            renderCanvas.Invalidate();
        }       
    }
}
