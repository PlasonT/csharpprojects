﻿using System;
using System.IO;

namespace Algorithm
{
    class Program
    {
        /// <summary>
        /// Утилита для простановки даты.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Указываем количество файлов.
            int filesQuantity = 5;

            // Обрабатываем документы пакетно.
            for (int index = 1; index <= filesQuantity; index++)
            {
                // Указываем расположение файлов (используем отностельный путь).
                string path = $"..\\..\\docs\\TextFile{index}.txt";

                // Проверяем, существуют ли файлы.
                if (File.Exists(path))
                {
                    // Сохраняем старое содержимое файла.
                    string oldText = File.ReadAllText(path);

                    // Открываем поток для записи в файл с запретом дозаписи.
                    using (var fs = new StreamWriter(path, false))
                    {
                        // Определяем строку, которую нужно записать.
                        string newParagraph = "дата ввода изменений - чт 03.12.2020";

                        // Добавляем в файл строку изменений.
                        fs.WriteLine(newParagraph);

                        // Добавляем старое содержание.
                        fs.WriteLine(oldText);                        
                    }                    
                }
                else
                {
                    // Выводим сообщение о невозможности записи.
                    Console.WriteLine("Такого файла не существует!");
                    Console.ReadKey();
                }
            }

            // Выводим сообщение об успешности операции.
            Console.WriteLine("Все файлы успешно записаны!");
            Console.ReadKey();
        }        
    }
}