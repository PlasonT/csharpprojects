﻿using System.Drawing;
using System.Windows.Forms;

namespace Geometry
{
    public partial class Form1 : Form
    {
        // Новые объекты.
        Line line;
        Rectangle box;
        Circle circle;        

        Bitmap bmp;
        Graphics graph;

        // Открытые свойства.
        public Pen RedColor { get; set; }
        public Pen BlackColor { get; set; }
        public Pen BlueColor { get; set; }
        public Pen GreenColor { get; set; }

        public Form1()
        {
            InitializeComponent(); 
            Init();
            Draw();
        }

        /// <summary>
        /// Инициализация рисования.
        /// </summary>
        private void Init()
        {
            bmp = new Bitmap(picture.Width, picture.Height);
            graph = Graphics.FromImage(bmp);

            // Используем свойства объекта (задаем цвет для фигур).
            RedColor = new Pen(Color.Red, 5);
            BlackColor = new Pen(Color.Black, 5);
            BlueColor = new Pen(Color.Blue, 5);
            GreenColor = new Pen(Color.Green, 8);

            line = new Line(100, 50, 300, 300);
            box = new Rectangle(100, 100, 250, 250);
            circle = new Circle(100, 100, 200, 200);
        }

        /// <summary>
        /// Метод построения геометрических фигур.
        /// </summary>
        private void Draw()
        {
            Draw(line);
            Draw(box);
            Draw(circle);

            picture.Image = bmp;
        }

        // Рисуем линию.
        private void Draw(Line line)
        {
            graph.DrawLine(RedColor, line.x1, line.y1, line.x2, line.y2);
        }

        // Рисуем прямоугольник.
        private void Draw(Rectangle box)
        {
            graph.DrawRectangle(BlackColor, box.x1, box.y1, box.width, box.height);
        }

        // Рисуем круг.
        private void Draw(Circle circle)
        {
            graph.DrawEllipse(BlueColor, circle.x, circle.y, circle.width, circle.height);
        }               
    }
}