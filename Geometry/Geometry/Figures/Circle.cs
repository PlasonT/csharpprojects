﻿namespace Geometry
{
    class Circle
    {
        public int x, y;
        public int width, height;

        public Circle(int x, int y, int width, int height)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }
}
