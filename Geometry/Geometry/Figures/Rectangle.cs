﻿namespace Geometry
{
    class Rectangle
    {
        public int x1, y1;
        public int width, height;

        public Rectangle(int x1, int y1, int width, int height)
        {
            this.x1 = x1;
            this.width= width;
            this.y1 = y1;
            this.height = height;
        }
    }
}
