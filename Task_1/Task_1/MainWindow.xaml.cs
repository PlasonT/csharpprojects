﻿using System;
using System.Drawing;
using System.Windows;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Task_1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void WindowsFormsHost_Initialized(object sender, EventArgs e)
        {
            renderCanvas.MakeCurrent();
        }

        private void RenderCanvas_Load(object sender, EventArgs e)
        {
            GL.ClearColor(System.Drawing.Color.White);
        }

        private void RenderCanvas_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            #region Setup

            // Очищаем буфер.
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Настотройки вида
            Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView((float)1.04, 4 / 3, 1, 10000);
            //Matrix4 ortho = Matrix4.CreateOrthographic(renderCanvas.Width, renderCanvas.Height, 10, 3000);
            Matrix4 lookAt = Matrix4.LookAt(100, 20, 0, 0, 0, 0, 0, 1, 0);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.LoadMatrix(ref perspective);
            // Загружаем камеру
            GL.MatrixMode(MatrixMode.Modelview); // загрузка камеры
            GL.LoadIdentity();
            GL.LoadMatrix(ref lookAt);

            //GL.Ortho(-50.0, 50.0, -50.0, 50.0, -50.0, 50.0);

            // Размер окна
            GL.Viewport(0, 0, renderCanvas.Width, renderCanvas.Height);

            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Less);

            #endregion

            #region Draw Triangle

            // Rotate
            GL.Rotate(0, 0, 0, 1);

            GL.Begin(PrimitiveType.Triangles);

            // Face1
            GL.Color3(Color.Red);
            GL.Vertex3(50, 0, 0);
            GL.Color3(Color.White);
            GL.Vertex3(0, 25, 0);
            GL.Color3(Color.Blue);
            GL.Vertex3(0, 0, 50);            

            // Face2
            GL.Color3(Color.Green);
            GL.Vertex3(-50, 0, 0);
            GL.Color3(Color.White);
            GL.Vertex3(0, 25, 0);
            GL.Color3(Color.Blue);
            GL.Vertex3(0, 0, 50);           

            // Face3
            GL.Color3(Color.Red);
            GL.Vertex3(50, 0, 0);
            GL.Color3(Color.White);
            GL.Vertex3(0, 25, 0);
            GL.Color3(Color.Blue);
            GL.Vertex3(0, 0, -50);            

            // Face4
            GL.Color3(Color.Green);
            GL.Vertex3(-50, 0, 0);
            GL.Color3(Color.White);
            GL.Vertex3(0, 25, 0);
            GL.Color3(Color.Blue);
            GL.Vertex3(0, 0, -50);            

            GL.End();

            #endregion

        }

        private void Size_textBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            renderCanvas.Invalidate();
        }

        private void Angle_textBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            renderCanvas.Invalidate();
        }        

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            // Закрываем окно
            this.Close();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {

            MessageBox.Show("Открываем триангуляцию", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Information);

        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Программа визуализации линии пересечения двух триангуляций", "О программе", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
